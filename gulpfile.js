var
  gulp         = require('gulp'),
  watch        = require('./solomon/tasks/watch'),
  build        = require('./solomon/tasks/build');


gulp.task('watch-solomon', watch);
gulp.task('build-solomon', build);
