from django.conf.urls import url
from pages import views
urlpatterns = [
    url(r'^$', views.home, name="home"),
    url(r'^que-creemos/', views.belief, name="que-creemos"),
    url(r'^programas/(?P<programLabel>[\w-]+)/$', views.programsDetail, name="detalle_programa"),
    url(r'^programas', views.programs, name="programas"),
    url(r'^faq/', views.faq, name="faq"),
    url(r'^donaciones/', views.donate, name="donaciones"),
    url(r'^contacto/', views.contact, name="contacto"),
    url(r'^testimonios/', views.testimonies, name="testimonios"),
]
