from django.contrib import admin
from .models import *
from django.forms import ModelForm, inlineformset_factory
from suit_redactor.widgets import RedactorWidget


class PageActionsInline(admin.StackedInline):
    model = Pages.actions.through
    verbose_name = u"Acción para pagina"
    extra = 5
    verbose_name_plural = u"Acciones para pagina"


@admin.register(PageActions)
class PageActionAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    verbose_name = u"Acción para pagina"
    verbose_name_plural = u"Acciones para pagina"


class PageItemsInline(admin.StackedInline):
    model = Pages.items.through
    verbose_name = u"Texto para pagina"
    extra = 5
    verbose_name_plural = u"Textos para pagina"


class PageItemForm(ModelForm):
    class Meta:
        widgets = {
            'text_english': RedactorWidget(
                editor_options={
                    'lang': 'es',
                    'overrideStyles': False,
                    'buttons': ['format', 'bold', 'italic', 'underline', 'deleted', 'lists', 'link', 'horizontalrule']
            }),
            'text_spanish': RedactorWidget(
                editor_options={
                    'lang': 'es',
                    'overrideStyles': False,
                    'buttons': ['format', 'bold', 'italic', 'underline', 'deleted', 'lists', 'link', 'horizontalrule']
            }),
        }

class PageItemAdmin(admin.ModelAdmin):
    form = PageItemForm
    list_display = ['name']
    list_filter = ['name']
    verbose_name = u"Texto para pagina"
    verbose_name_plural = u"Textos para pagina"

admin.site.register(PageItems, PageItemAdmin)

class PageImagesInline(admin.StackedInline):
    model = Pages.images.through
    verbose_name = u"Imagen para pagina"
    extra = 5
    verbose_name_plural = u"Imagenes para pagina"


@admin.register(PageImages)
class PageImageAdmin(admin.ModelAdmin):
    list_display = ['image']
    list_filter = ['image']
    verbose_name = u"Imagen para pagina"
    verbose_name_plural = u"Imagenes para pagina"


@admin.register(Pages)
class PagesAdmin(admin.ModelAdmin):
    list_display = ['name', 'summary', 'group']
    list_filter = ['name', 'group']
    exclude = ['items', 'images', 'actions']

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.inlines = (PageImagesInline, PageItemsInline, PageActionsInline)
        return super(PagesAdmin, self).change_view(request, object_id)


@admin.register(PageItemGroups)
class PageItemGroupsAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    verbose_name = u"Grupo de contenido"
    verbose_name_plural = u"Grupos de contenido"


@admin.register(PageGroups)
class PageGroupsAdmin(admin.ModelAdmin):
    list_display = ['label']
    list_filter = ['label']
    verbose_name = u"Grupo de paginas"
    verbose_name_plural = u"Grupos de paginas"
