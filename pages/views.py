from django.shortcuts import render
from django.utils.translation import get_language_from_request
from pages.models import Pages, PageItemGroups
from programs.models import Programs
from pages.forms import ContactForm
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template import Context
from django.template.loader import get_template
# Create your views here.
def home(request):
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    pageObjects = Pages.objects.get(label="home")
    data = setBasicData(data, pageObjects, userLang, userLocale, False)
    data['features'] = groupContent(pageObjects, userLang, 'caracteristica_')
    data['slides'] = groupContent(pageObjects, userLang, 'home_slider')
    data['section_one'] = groupContent(pageObjects, userLang, 'home_section_one')[0]
    data['section_two'] = groupContent(pageObjects, userLang, 'home_section_two')[0]
    data['section_three'] = groupContent(pageObjects, userLang, 'seccion_caracteristicas')[0]
    return render(request, 'home/index.html',data)

def belief(request):
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    pageObjects = Pages.objects.get(label="que-creemos")
    data = setBasicData(data, pageObjects, userLang, userLocale, 'cover_creemos')
    data['tabs']= []
    for num in range(1, 11):
        tabGroup = 'tab_doctrina_' + str(num)
        tabContent = groupContent(pageObjects, userLang, tabGroup)[0]
        data['tabs'].append(tabContent)
    return render(request, 'belief/index.html',data)

def programs(request):
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    pageObjects = Pages.objects.get(label="programas")
    data = setBasicData(data, pageObjects, userLang, userLocale, 'cover_programas')
    data['programs'] = Programs.objects.all()
    return render(request, 'programs/index.html',data)

def programsDetail(request, programLabel):
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    data = setMetas(data, userLang, userLocale)
    data['program'] = Programs.objects.get(label = programLabel)
    data['academicModules'] = data['program'].modules
    data['label'] = data['program'].label
    data['title'] = data['program'].name if userLang == 'es' else data['program'].name_en
    data['description'] = data['program'].description if userLang == 'es' else data['program'].description_en
    mainPage = Pages.objects.get(label="programas")
    data['programs'] = Programs.objects.exclude(label = programLabel)
    data['section_one'] = groupContent(mainPage, userLang, 'seccion_caracteristicas')[0]
    data['features'] = groupContent(mainPage, userLang, 'caracteristica_')
    return render(request, 'programs/detail.html',data)


def faq(request):
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    pageObjects = Pages.objects.get(label="faq")
    data = setBasicData(data, pageObjects, userLang, userLocale, 'cover_faq')
    data['questions']= []
    counter = 1
    while counter != 0:
        try:
            tabGroup = 'faq_' + str(counter)
            tabContent = groupContent(pageObjects, userLang, tabGroup)[0]
            data['questions'].append(tabContent)
            counter = counter + 1
        except:
            counter = 0
    return render(request, 'faq/index.html', data)

def donate(request):
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    pageObjects = Pages.objects.get(label="donaciones")
    data = setBasicData(data, pageObjects, userLang, userLocale, 'cover_donate')
    return render(request, 'donate/index.html',data)

def contact(request):
    form_class = ContactForm
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    pageObjects = Pages.objects.get(label="contacto")
    data = setBasicData(data, pageObjects, userLang, userLocale, 'cover_contact')
    data['contactForm'] = form_class
    # new logic!
    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = request.POST.get(
                'contact_name'
            , '')
            contact_email = request.POST.get(
                'contact_email'
            , '')
            form_content = request.POST.get('content', '')

            # Email the profile with the
            # contact information
            template = get_template('email/contact_template.txt')
            context = {
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            }
            content = template.render(context)

            email = EmailMessage(
                "Nuevo correo de contacto",
                content,
                "instituto Fiel" +'',
                ['comunicaciones@institutofiel.com'],
                headers = {'Reply-To': contact_email }
            )
            email.send()
            return redirect('contact')
    return render(request, 'contact/index.html',data)

def testimonies(request):
    data= {}
    userLocale = simpleLocale(request)
    userLang = simpleLang(userLocale)
    pageObjects = Pages.objects.get(label="testimonios")
    data = setBasicData(data, pageObjects, userLang, userLocale, 'cover_testimonios')
    return render(request, 'testimonies/index.html',data)

def simpleLocale(request):
    loc = get_language_from_request(request, False)
    if 'es' in loc:
        return loc
    else:
        return 'en-us'

def simpleLang(lang):
    if 'es' in lang:
        return 'es'
    else:
        return 'en'

def setBasicData(data, page, lang, locale, coverName):
    data['title'] = page.name if  lang == 'es' else page.name_en
    data['description'] = page.summary if  lang == 'es' else page.summary_en
    data = setMetas(data, lang, locale)
    if coverName:
        data['cover'] = groupContent(page, lang, coverName)[0]
    return data;

def setMetas(data, lang, locale):
    data['lang'] = lang
    data['locale'] = locale
    data['menu'] = getMenuItems(lang)
    return data

def groupContent(pageObjects, lang, groupLabel):
    items = []
    itemGroups = PageItemGroups.objects.filter(name__startswith=groupLabel)
    for group in itemGroups:
        item = {}
        item['id'] = group.name
        for a in pageObjects.actions.all() :
            if a.group.name == group.name:
                item['cta'] = a
        for it in pageObjects.items.all() :
            if it.group.name == group.name and 'title_' in it.name and not 'subtitle_' in it.name:
                item['title'] = it.text_spanish or None if lang == 'es' else it.text_english or None
            if it.group.name == group.name and 'subtitle_' in it.name:
                item['subtitle'] = it.text_spanish or None if lang == 'es' else it.text_english or None
            if it.group.name == group.name and 'text_' in it.name:
                item['text'] = it.text_spanish or None if lang == 'es' else it.text_english or None
        for img in pageObjects.images.all():
            if img.group.name==group.name:
                item['image'] = img
        items.append(item)
    return items

def getMenuItems(lang):
    menuItems = []
    pages = Pages.objects.order_by('id').exclude(label="home")
    for p in pages:
        pageItem = {}
        pageItem['route'] = p.label
        pageItem['name'] =  p.name if lang == 'es' else p.name_en
        if p.group:
            groupExists = next((i for i, item in enumerate(menuItems) if 'group' in item and item['group'] == p.group.label), -1)
            if groupExists == -1:
                groupItem = {}
                groupItem['group'] = p.group.label
                groupItem['name'] = p.group.name if lang == 'es' else p.group.name_en
                groupItem['subitems'] = []
                groupItem['subitems'].append(pageItem)
                menuItems.append(groupItem)
            else:
                selectedGroup = menuItems[groupExists]
                selectedGroup['subitems'].append(pageItem)
        else:
            menuItems.append(pageItem)
    return menuItems
