from django.db import models
class Pages(models.Model):
    #Generales
    label = models.CharField(
        max_length=80,
        verbose_name=u'Label',
        help_text=u'Nombre unico de la pagina (despues de creado nunca cambiar)',
        null=True
    )
    group = models.ForeignKey('pages.PageGroups',
        blank=True,
        null=True,
        verbose_name=u'Grupo',
        help_text=u'Solo usar si se necesita agrupar la pagina dentro de un grupo de paginas ej. menus dropdown'
    )
    name = models.CharField(
        max_length=80,
        verbose_name=u'Titulo pagina  (Español)',
        help_text=u'Titulo de la pagina',
        null=True
    )
    name_en = models.CharField(
        max_length=80,
        verbose_name=u'Titulo pagina (Ingles)',
        help_text=u'Titulo de la pagina',
        null=True
    )
    summary = models.CharField(
        max_length=140,
        verbose_name=u'Resumen de la pagina (Español)',
        help_text=u'Este resumen ayuda a indexar la pagina en buscadores y al compartir a traves de redes sociales. Es el primer texto que la gente vera'
    )
    summary_en = models.CharField(
        max_length=140,
        verbose_name=u'Resumen de la pagina (Ingles)',
        help_text=u'Este resumen ayuda a indexar la pagina en buscadores y al compartir a traves de redes sociales. Es el primer texto que la gente vera'
        )
    items = models.ManyToManyField(
        'pages.PageItems',
        related_name='pageItems',
        verbose_name=u'Textos que incluye la pagina',
        blank=True
    )
    images = models.ManyToManyField(
        'pages.PageImages',
        related_name='pageImages',
        verbose_name=u'Imagenes que incluye la pagina',
        blank=True
    )
    actions = models.ManyToManyField(
        'pages.PageActions',
        related_name='pageActions',
        verbose_name=u'Acciones que incluye la pagina',
        blank=True
    )
    def __str__(self):
        return u'%s' % (self.name)

    class Meta:
        verbose_name = u'Pagina publica'
        verbose_name_plural = u' Paginas publicas'
        ordering = ('-id',)
        get_latest_by = 'id'

class PageItems(models.Model):
    group = models.ForeignKey('pages.PageItemGroups', blank=True, null=True, verbose_name=u'Grupo')
    name = models.CharField(
        max_length=80,
        verbose_name=u'Nombre del texto',
        help_text=u'Nombre que ayuda a identificar los textos de la pagina'
    )
    text_english = models.TextField(
        verbose_name=u'Texto en ingles',
    )
    text_spanish = models.TextField(
        verbose_name=u'Texto en español',
    )
    def __str__(self):
        return u'%s' % (self.name)
    class Meta:
        verbose_name = u'Texto pagina'
        verbose_name_plural = u' Textos para paginas'
        ordering = ('-id',)
        get_latest_by = 'id'

class PageActions(models.Model):
    group = models.ForeignKey('pages.PageItemGroups', blank=True, null=True, verbose_name=u'Grupo')
    name = models.CharField(
        max_length=80,
        verbose_name=u'Nombre del texto',
        help_text=u'Nombre que ayuda a identificar los textos de la pagina'
    )
    url = models.CharField(
        max_length=80,
        verbose_name=u'Label de url',
        help_text=u'Escriba aca el nombre (label) que identifica la pagina a la que este boton redireccionara '
    )
    text_english = models.TextField(
        verbose_name=u'Texto en ingles',
    )
    text_spanish = models.TextField(
        verbose_name=u'Texto en español',
    )
    def __str__(self):
        return u'%s %s' % (self.name, self.url)
    class Meta:
        verbose_name = u'Acción'
        verbose_name_plural = u' Acciones'
        ordering = ('-id',)
        get_latest_by = 'id'

class PageImages(models.Model):
    label = models.CharField(
        max_length=80,
        verbose_name=u'Label',
        help_text=u'Nombre unico de la pagina (despues de creado nunca cambiar)',
        null=True
    )
    group = models.ForeignKey('pages.PageItemGroups', blank=True, null=True, verbose_name=u'Grupo')
    text_english = models.TextField(
        verbose_name=u'Descripción imagen ingles',
    )
    text_spanish = models.TextField(
        verbose_name=u'Descripción imagen español',
    )
    image = models.FileField(
    upload_to=u'page-images',
    verbose_name=u'Imagen'
    )
    def __str__(self):
        return u'%s' % (self.label)
    class Meta:
        verbose_name = u'Imagen pagina'
        verbose_name_plural = u' Imagenes para paginas'
        ordering = ('-id',)
        get_latest_by = 'id'

class PageItemGroups(models.Model):
    name = models.CharField(
        max_length=80,
        verbose_name=u'Label',
        help_text=u'Nombre del grupo unido por guion bajo ej. home_carrusel'
    )
    def __str__(self):
        return self.name
    class Meta:
        verbose_name = u"Grupo de contenido"
        verbose_name_plural = u"Grupos de contenido"
        ordering = ('id',)
        get_latest_by = 'id'
class PageGroups(models.Model):
    label = models.CharField(
        max_length=80,
        verbose_name=u'Label',
        help_text=u'Nombre unico del grupo (despues de creado nunca cambiar)',
        null=True
    )
    name = models.CharField(
        max_length=80,
        verbose_name=u'Nombre del grupo de paginas (español)',
        help_text=u'Nombre del grupo unido por guion bajo ej. institucion'
    )
    name_en = models.CharField(
        max_length=80,
        verbose_name=u'Nombre del grupo de paginas (ingles)',
        help_text=u'Nombre del grupo unido por guion bajo ej. institucion'
    )
    def __str__(self):
        return self.label
    class Meta:
        verbose_name = u"Grupo de paginas"
        verbose_name_plural = u"Grupos de paginas"
        ordering = ('-id',)
        get_latest_by = 'id'
