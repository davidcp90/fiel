# Fiel Webapp
Take a look to this guide to start developing features for this app.
#### Folder structure
* bezalel -> Design files
* solomon -> UI system
* media -> Media uploaded by the webapp
* fiel -> Main django app. Bootstraps the application
* fiel-env -> This is your personal python virtual environment. Is excluded from version control and you have to install it manually. (See installation instructions)
* fixtures -> Json based data dumps with data useful to preload your database

Other folders correspond to django apps


#### Install
Add your ssh key to gitlab and clone the repository
```sh
$ git clone git@gitlab.com:institutofiel/fiel.git
```
Make sure you have python 3.6, node and npm installed and execute the following command. This will create a folder called *fiel-env* with a virtual environment suitable to manage all the dependecies required for the project
```sh
$ python3.6 -m venv fiel-env
```
Be sure to have your virtual-env activated (you can noticed it because a `(fiel-env)`is going to appear before your username on the terminal. If not you can run (once in the dir):
```sh
$ source fiel-env/bin/activate
```
Install required packages
```sh
$ npm i
$ pip install -r requirements.txt
```
Create a mysql local db
```sh
$ mysql -u root
> create user 'fiel' identified by 'NeHeMiaH2000--Fiel';
> grant all privileges on fiel_database.* to 'fiel';
```
Run db migrations
```sh
$ ./manage.py migrate
```
Load initial db fixtures
```sh
$ ./manage.py loaddata fixtures/initialdata.json
```
Create a superuser for yourself
```sh
$ ./manage.py createsuperuser
```
Now you can start playing with the app. Try running it
```sh
$ ./manage.py runserver 7000
```
Then go to http://localhost:7000/admin and try to log-in with the user you created.
#### Guidelines
**GIT** Inside this repo you are going to find the file [git-tips.pdf](https://gitlab.com/institutofiel/fiel/blob/master/git-tips.pdf) which explain good practices and a comprehensive git workflow. To be friendly with your fellow modern scribes take a look to this [article to write better commit messages](https://chris.beams.io/posts/git-commit/). Forgive me for not following this at the quickstart of this project.
**Django** [Django styleguide](https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/)
**BEM** I used BEM for CSS naming and to discern between our custom classes and our CSS framework classes. [Take a look to BEM](http://getbem.com/)
#### Server configuration guide
In case you want to know or you are going to setup a live server. You can follow the next guide for it:
[Setup django live server](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-16-04#create-a-gunicorn-systemd-service-file)
_NOTE_: Be aware that the guide was written for postgresql and we are using mysql.
#### TODO
* Setup a task runner for python commands
* Separate css into files and use gulp to automate the process
* Setup automatic deployment
