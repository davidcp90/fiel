from django.db import models

class Programs(models.Model):
    #Generales
    label = models.CharField(
        max_length=80,
        verbose_name=u'Label',
        help_text=u'Nombre unico del programa (despues de creado nunca cambiar)',
        null=False
    )
    name = models.CharField(
        max_length=80,
        verbose_name=u'Titulo pagina  (Español)',
        help_text=u'Titulo de la pagina',
        null=True
    )
    name_en = models.CharField(
        max_length=80,
        verbose_name=u'Titulo pagina (Ingles)',
        help_text=u'Titulo de la pagina',
        null=True
    )
    description = models.CharField(
        max_length=140,
        verbose_name=u'Descripción del programa (Español)'
    )
    description_en = models.CharField(
        max_length=140,
        verbose_name=u'Descripción del programa (Ingles)'
    )
    inscription_date = models.DateTimeField(verbose_name=u'Fecha maxima de inscripción',
                                      auto_now_add=True, blank=True, null=True)
    price = models.DecimalField(max_digits=12, decimal_places=2, default=0,verbose_name=u'Precio programa (en pesos colombianos)')
    duration = models.DecimalField(max_digits=12, decimal_places=2, default=0,verbose_name=u'Duración en semanas')
    image = models.FileField(
        upload_to=u'programs-images',
        verbose_name=u'Imagen representativa'
    )
    logo = models.FileField(
        upload_to=u'programs-images',
        verbose_name=u'logo del programa',
        blank=True,
        null=True
    )
    modules = models.ManyToManyField(
        'programs.AcademicModules',
        related_name='programAcademicModules',
        verbose_name=u'Modulos que incluye el programa',
        blank=True
    )
    teachers = models.ManyToManyField(
        'profiles.Teacher',
        related_name='programTeacher',
        verbose_name=u'Profesores',
        blank=True
    )
    def __str__(self):
        return u'%s' % (self.name)

    class Meta:
        verbose_name = u'Programa'
        verbose_name_plural = u' Programas academicos'
        ordering = ('-id',)
        get_latest_by = 'id'

class AcademicModules(models.Model):
    name = models.CharField(
        max_length=80,
        verbose_name=u'Titulo modulo  (Español)',
        help_text=u'Titulo del modulo',
        null=True
    )
    name_en = models.CharField(
        max_length=80,
        verbose_name=u'Titulo modulo (Ingles)',
        help_text=u'Titulo del modulo',
        null=True
    )
    description = models.CharField(
        max_length=500,
        verbose_name=u'Descripción del modulo (Español)'
    )
    description_en = models.CharField(
        max_length=500,
        verbose_name=u'Descripción del modulo (Ingles)'
    )
    duration = models.DecimalField(max_digits=12, decimal_places=2, default=0,verbose_name=u'Duración en horas')
    subjects = models.ManyToManyField(
        'programs.Subjects',
        related_name='programSubjects',
        verbose_name=u'Materias que incluye el modulo',
        blank=True
    )
    def __str__(self):
        return u'%s' % (self.name)

    class Meta:
        verbose_name = u'Modulo'
        verbose_name_plural = u' Modulos'
        ordering = ('-id',)
        get_latest_by = 'id'

class Subjects(models.Model):
    name = models.CharField(
        max_length=80,
        verbose_name=u'Titulo asignatura  (Español)',
        null=True
    )
    name_en = models.CharField(
        max_length=80,
        verbose_name=u'Titulo asignatura (Ingles)',
        null=True
    )
    def __str__(self):
        return u'%s' % (self.name)

    class Meta:
        verbose_name = u'Asignatura'
        verbose_name_plural = u'Asignaturas'
        ordering = ('-id',)
        get_latest_by = 'id'
