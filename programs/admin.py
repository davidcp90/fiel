from django.contrib import admin
from .models import *
from django.forms import ModelForm, inlineformset_factory


class TeachersInline(admin.StackedInline):
    model = Programs.teachers.through
    verbose_name = u"Profesor"
    extra = 2
    verbose_name_plural = u"Profesores"

class SubjectsInline(admin.StackedInline):
    model = Programs.modules.through
    verbose_name = u"Asignatura"
    extra = 5
    verbose_name_plural = u"Asignaturas"

@admin.register(Subjects)
class SubjectsAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    verbose_name = u"Asignatura"
    verbose_name_plural = u"Asignaturas"

class AcademicModulesInline(admin.StackedInline):
    model = Programs.modules.through
    verbose_name = u"Modulo"
    extra = 5
    verbose_name_plural = u"Modulos"

@admin.register(AcademicModules)
class AcademicModulesAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    verbose_name = u"Modulo"
    verbose_name_plural = u"Modulos"

@admin.register(Programs)
class ProgramsAdmin(admin.ModelAdmin):
    list_display = ['name']
    list_filter = ['name']
    exclude = ['modules', 'teachers']
    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.inlines = (AcademicModulesInline, TeachersInline)
        return super(ProgramsAdmin, self).change_view(request, object_id)
